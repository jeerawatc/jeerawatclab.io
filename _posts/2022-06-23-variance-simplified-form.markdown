---
layout: post
title:  "Simplified Form for Variance Calculation"
date:   2022-07-08 16:10:36 +0700
categories: stats maths proofs
---

The variance of a random variable is the measure of spread. Variance is defined as

$$
Var(X) = E[(X - \mu)^2]
$$

From what I've learned, this formula is harder to used. Although I haven't gone through the pain of integration yet so I can't say for certain, I'll believe my sensei. So the formula can be simplified for easier calculation. From my calculation, it goes

$$\begin{align}
Var(X) & = E[(X - \mu)^2]  \\
& = E[X^2 - 2X\mu + \mu^2] \\
& = E[X^2] - E[2X\mu] + E[\mu^2] \\
& = E[X^2] - 2\mu E[X] + \mu^2 \\
& = E[X^2] - 2E[X]E[X] + E[X]^2\ (\because E[X] = \mu)\\
& = E[X^2] - 2E[X]^2 + E[X]^2 \\
& = E[X^2] - E[X]^2 \\
& Q.E.D. 🎉
\end{align}$$

This should be correct. What I can try to learn next is the integral form of a function of a random variable and the reason why the derivative is with respect to that random variable only and not to the function. Maybe I need to understand expected value better 😞. 

Expected of $ X^2 $ is

$$ 
E[X^2] =  \int_{-\infty}^{\infty} x^2 f(x) \,dx 
$$

but if look in term of $ g(x) = x^2 $, the following should also be correct

$$ 
E[X^2] =  \int_{-\infty}^{\infty} x^2 f(x^2) \,d(x^2) 
$$

Maybe it's the same 🤔? I'll share my calculation in the next post. Might need to brush up my calculus skill first.

Until next time,<br>
Jeerawat