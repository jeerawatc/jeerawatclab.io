---
layout: post
title:  "MLOps Stack To Try"
date:   2022-08-31 09:37:37 +0700
categories: mlops pipeline
---

I'm taking a break from my already light sessions of maths studies 😂. I have stumbled upon an interesting website that aggregates many interesting mlops tools with various capabilities from pipelining, orchastration, training, and serving. I'm already familiar with some of the tools since I have to use them for work. However, there are some gaps that I think can me better managed with the suitable tools. Also, some of the tools that I have touched from work are still hard to use for me. If I can implement a stack it would help me improve my skills as a data scientist a lot.

The mentioned website is [MyMLOps](https://mymlops.com/). It's really useful since the website is interactive with the tools selection and the data science general workflow is already put in place. Our selections will also be shown in a beautiful graph down below. Here is the stack that I have chosen.

![mlops-stack](/img/2022-08-31-mlops-stack-to-try/mlops-stack.jpg)

The first step is of course experimentation. A couple IPython notebooks to show the data analysis, model experimentations, and evaluations. The experimentations will be tracked using MLflow with the artifacts being metrics, plots, test dataset or if the data is quite big then the indices, and model objects. I'm not familiar with data versioning that much and I'm still not sure if it requires you to know about databases 😐. Anyhow, DVC should helps a ton with data versioning if I start with a small dataset and should also be not too hard to grasp since it's a bit like git. Dask handles parallel computing. Not that we need it but a good addition. Everything must be tracked using git obviously. The star of the show is ZenML because I'm really curious about the workings of pipeline orchastration tools in ML. My guess is that it's Apache Airflow for ML but we'll see. Model serving is done using Seldon Core which obtains models from MLflow model registry. The optional tool for this pipeline but essential for any ML pipeline is the data monitoring tool. I'll use Evidently.

The pipeline I designed should be able to handle most of the mid-size data product lifecycles. The hardest part is going to be deployment since I'm new to getting this many parts working together. Hope ZenML and Seldon Core won't be too difficult 🥲. I'll see if there are anything worth sharing in this blog.

Until next time,<br>
Jeerawat