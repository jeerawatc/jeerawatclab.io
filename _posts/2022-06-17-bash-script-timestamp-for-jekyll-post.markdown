---
layout: post
title:  "Bash Script for Generating Timestamp in the format for Posts in Jekyll"
date:   2022-06-17 01:00:06 +0700
categories: snippet bash
---

The format for Jekyll posts is `YYYY-MM-DD HH:mm:ss +hhmm`. I can't seem to find the website that can generate timestamp string in this format. The website `https://www.utctime.net/` is the closest but no option to change the numeric time zone. Even with `https://www.utctime.net/tha-time-now` the numeric time zone seems to be adjusted to the Thailand time zone and show up as `+0000` anyways 😞.

The solution I'm going for is ✨bash script✨. Today I learned that you can get a string of a timestamp using `date` command in bash. The format can be set using the `[+FORMAT]` argument. To get the aforementioned format, the command I have to use is

```bash
date +"%Y-%m-%d %X %z"
```

Since bash supports command aliasing, I added the following alias to `~/.bashrc` to easily get the time from just a short command 😋.

```bash
alias datenow='date +"%Y-%m-%d %X %z"'
```

Next thing I can do is add a command to generate a new markdown file with the format of a Jekyll post. Bash should be able to do the job. As for now, enough scipting. `datenow` is telling me it's too late right now. Time to go to sleep 🛏️.

Until next time, <br>
Jeerawat