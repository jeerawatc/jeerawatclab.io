---
layout: post
title:  "Sample Variance and the Mystery of the Denominator"
date:   2022-09-01 23:47:34 +0700
categories: stats maths proofs
---

Variance is a measure of dispersion. That should be fairly familiar to anyone who have the chance to get their hands on some statistical goodness 😋. Wikipedia's article on variance states the definition of variance as follows.

> The variance of a random variable $X$ is the expected value of the squared deviation from the mean of $X$

From the definition, we can write it mathematically so.

$$\begin{align}
Var(X) = E[(X - \mu)^2]
\end{align}$$

Let $n$ be the amount of observations and $\mu$ be the population mean. In the case that all of the population observations is known, we can calculate the population variance using the formula below.

$$\begin{align}
\sigma^2 = \frac{1}{n} \sum_{i=1}^{n}(x_i - \mu)^2
\end{align}$$

However, you might get to encounter the calculation of variance from sampled data that looks similar to the one used with population data but with a little twist. The denominator is $n-1$. Let $S$ be the sample standard deviation and $\bar{x}$ be the sample mean, the formula is as follows.

$$\begin{align}
s^2 = \frac{1}{n-1} \sum_{i=1}^{n}(x_i - \bar{x})^2
\end{align}$$

The reason that the sample variance formula needs to use $n-1$ as a denominator is because it makes the estimation of variance _unbiased_. We can proved this by using the simplified form for variance calculation in an older post and some properties of expected values. It's mathing time 🦇. But first we have to state some definitions and properties so that it's easier to follow.

Definition of sample mean:

$$\begin{align}  
\bar{x} = \frac{1}{n} \sum_{i=1}^{n}x_i 
\end{align} $$

$$\begin{align}  
\sum_{i=1}^{n}x_i = n\bar{x}
\end{align} $$

Variance in the simplified form:

$$\begin{align} 
Var(X) = E[X^2] - E[X]^2 
\end{align}$$

$$\begin{align} 
E[X^2] = Var(X) + E[X]^2 
\end{align}$$

Another essential mini-proof that can help is how to change the numerator to a more convenient form.

$$\begin{align}
\sum_{i=1}^{n}(x_i - \bar{x})^2 & = \sum_{i=1}^{n}(x_i^2 - 2x_i\bar{x} + \bar{x}^2) \\
& = \sum_{i=1}^{n}x_i^2 - 2\bar{x}\sum_{i=1}^{n}x_i + n\bar{x} \\
& = \sum_{i=1}^{n}x_i^2 - 2n\bar{x}^2 + n\bar{x}^2 && \text{sub. from (5)} \\
& = \sum_{i=1}^{n}x_i^2 - n\bar{x}^2
\end{align}$$

Now that the numerator is factored out into two terms, we can get into the main proof we're here for by starting at the expected value of the sum of squares.


$$\begin{align}
E[\sum_{i=1}^{n}(x_i - \bar{x}^2)] & = \sum_{i=1}^{n}E[x_i^2] - nE[\bar{x}^2] \\
& = \sum_{i=1}^{n}\{Var(x_i) + \mu^2\} - n\{Var(\bar{x} + \mu^2)\} && \text{sub. from (7)} \\
& = \sum_{i=1}^{n}(\sigma^2 + \mu^2) - n(\frac{\sigma^2}{n} + \mu^2) \\
& = n\sigma^2 + n\mu^2 - \sigma^2 - n\mu^2 \\
& = (n-1)\sigma^2
\end{align}$$

We can rearrange the terms and reveal the truth 😊.

$$\begin{align}
\sigma^2 & = E[\frac{1}{n-1}\sum_{i=1}^{n}(x_i - \bar{x}^2)] \\
& = E[s^2] && \text{sub. from (3)}
\end{align}$$

The population variance can be estimated by using the sample variance formula which has $n-1$ as the denomitor!

That concludes this blog post. That was quite a journey don't you think? It required understandings of algebra, summation properties, expected value, and a bunch of other definitions. I hope I didn't make any mistake in the calculation but feel free to notify me if you notice any. Hope you have fun and see you on the next post. Let's see if we'll get down the maths path or the MLOps path but should be fun either way 😘.

Until next time, \
Jeerawat
